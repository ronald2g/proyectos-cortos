#include <fstream>
#include <iostream>
#include <memory>
#include <stdlib.h> 
#include <stdio.h>
#include <string.h>

using namespace std;

bool hasEnding (std::string const &fullString, std::string const &ending)
{
	if (fullString.length() >= ending.length()) {
		return (0 == fullString.compare (fullString.length() - ending.length(), ending.length(), ending));
	}
	else
	{
		return false;
	}
}


void readData(ifstream& in,ofstream& out,string nombre)
{
	std::string buscar = " * @ORM\Entity";
	if (in.is_open())
	{
		std::string line;
		while (std::getline(in, line))
		{
			if(line.rfind(" * @ORM\\Entity", 0) == 0)
			{
				out << " * @ORM\\Entity(repositoryClass=\"App\\Repository\\" << nombre << "Repository\")" << endl;
			}
			else
				out << line << endl;
		}
		in.close();
	}
}

/**
  * @desc Añade Repositorios a las Entidades de Symfony
  * @param nombre de la Entidad
  * @return la Entidad mas un simbolo de subrayado
*/

// para renombrar los entidades a su nombre original
// for i in *.php; do mv -f $i"_" $i; done

int main(int argc, char** argv)
{
	if ( argc > 1 )
	{
		std::string salida(argv[1]);
		if (hasEnding(salida,".php"))
		{
			salida += "_";
			ifstream ifile(argv[1]);
			ofstream ofile(salida.c_str());
			string nombre = salida.substr(0 , salida.size()-5);
			if ( ifile && ofile )
			{
				readData(ifile,ofile,nombre);
			}
		}
	}
}
