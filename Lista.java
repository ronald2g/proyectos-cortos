class Lista extends Elemento
{
  Elemento  valor;
  Lista     siguiente;

  Lista(Elemento valor,Lista siguiente)
  {
    this.valor     = valor;
    this.siguiente = siguiente;
  }
 
  String mostrar()
  {
    String suma="[";
    suma.concat(this.valor.mostrar());
    if(this.siguiente!=null) suma.concat(this.siguiente.mostrar());
    else suma.concat("]");
    return suma;
  }

  Elemento lugar(int indice)
  {
    if(indice==0)return this.valor;
    else if(this.siguiente!=null) return this.siguiente.lugar(indice-1);
    else return null;
   }
}


class Propiedad extends Elemento
{
  String     nombre;
  Elemento   valor;
  Propiedad  siguiente;
 
 Propiedad(String nombre,Elemento valor,Propiedad siguiente) 
 { 
  this.nombre    = nombre; 
  this.valor     = valor; 
  this.siguiente = siguiente;
 }

 String mostrar()
 {
  String suma="{"+this.nombre+"|";
  suma.concat(this.valor.mostrar());
  if(this.siguiente==null) suma.concat("}");
  else suma.concat(this.siguiente.mostrar());
  return suma;
 }

 Elemento indice(String nombre)
 {
  if(this.nombre.equals(nombre))return this.valor;
  else if(this.siguiente!=null) return this.siguiente.indice(nombre);
  else return null;
 }

 Elemento lugar(int indice)
    {
        if(indice==0)return this.valor;
        else if(this.siguiente!=null) return this.siguiente.lugar(indice-1);
        else return null;
    }

}; 




class Elemento
{
 String mostrar(){};
 Elemento indice(String nombre){return null;};
 Elemento lugar(int indice){return null;};
 Elemento leer(String a,int lugar)
    {
        int inicio=lugar;
        switch(a.charAt(inicio))
        {
            case '{':
            {
                lugar=a.find('|',inicio+1)+1;
                String nombre=a.substr(inicio+1,lugar-inicio-2);
                return new Propiedad(nombre,leer(a,lugar),(Propiedad )leer(a,lugar));
                break;
            }
            case '[':
            {
                lugar++;
                return new Lista(leer(a,lugar),(Lista )leer(a,lugar));
                break;
            }
            case ']': case '}':
            {
                lugar++;
                return null;
                break;
            }
            default:
            {
                lugar=a.find_first_of(separador,inicio+1);
                return new Cadena(a.substr(inicio,lugar-inicio));
            }
        }
    }

} 

class Cadena extends Elemento
{

  String valor;

 Cadena(String valor)
 {
  this.valor=valor;
 }

 String mostrar()
 {
  return this.valor;
 }

}






/*
int main() 
{ 
/
    Propiedad p = new Propiedad("Honda",new Propiedad("x",new Lista(new Cadena("22"),new Lista(new Cadena("31"),new Lista(new Cadena("11"),null))),null), new Propiedad("que",new Cadena("45"), new Propiedad("sal",new Cadena("64"),null)));
    Propiedad x = new Propiedad("Honda",new Propiedad("x",new Cadena("32"),new Propiedad("que",new Cadena("45"), new Propiedad("sal",new Cadena("64"),null))),null);
/
    String a="{Honda|{x|[22[31[11]}{que|45{sal|64}";
    //String a="{honda|23}";
 String b="{Honda|{x|32{que|45{sal|64}}";
 int lug=0;
 Elemento p=Elemento::leer(a,lug);
 cout<<p.indice("Honda").mostrar()<<endl;
 cout<<endl<<"resultado"<<endl;
 cout<<p.mostrar();
 cout<<endl;
} 

*/